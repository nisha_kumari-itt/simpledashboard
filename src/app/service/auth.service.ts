import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../Interfac.';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: User = {
    fullname: '',
    emailId: '',
    phonenumber: 0,
    password: '',
    cpassword: '',
    id: 0,
    inlineRadioOption: '',
  };
  User = new Subject<User>();
  constructor(private http: HttpClient, private router: Router) {}
  login(loginForm: NgForm) {
    this.http.get(environment.API_PREFIX + '/SignupUsers').subscribe(
      (res) => {
        const data = <User[]>res;
        data.forEach((ele) => {
          if (
            ele.emailId === loginForm.value.email &&
            ele.password === loginForm.value.password
          ) {
            this.user = ele;
            this.router.navigate(['/dashboard']);
            localStorage.setItem('user', JSON.stringify(this.user));
          }
        });
      },
      (err) => {
        alert('Failed to Signup');
      }
    );
  }
}
