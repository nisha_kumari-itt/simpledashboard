import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EmployeeInterface } from '../Interfac.';
@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  Employees = new Subject<EmployeeInterface[]>();
  employees: EmployeeInterface[] = [];
  employee = new Subject<EmployeeInterface>();
  constructor(private http: HttpClient, private router: Router) {}
  postEmployee(data: any) {
    return this.http.post(environment.API_PREFIX + '/Employee', data);
  }
  getEmployees() {
    this.http.get(environment.API_PREFIX + '/Employee').subscribe((res) => {
      this.Employees.next(<EmployeeInterface[]>res);
      this.employees = <EmployeeInterface[]>res;
    });
  }
  getEmployeeById(id: number) {
    console.log(id);
    this.employees.forEach((ele) => {
      if (ele.id == id) {
        this.employee.next(ele);
      }
    });
  }
  getEmployeeByEmail(email: string) {
    console.log('input email is ' + email);
    let counter: number = 0;
    this.employees.forEach((ele) => {
      if (email === ele.email) {
        counter++;
      }
    });
    if (counter == 0) {
      return true;
    } else {
      return false;
    }
  }
  updateEmployee(data: EmployeeInterface, id: number) {
    this.http
      .put(environment.API_PREFIX + '/Employee/' + id, data)
      .subscribe((res) => {
        this.router.navigate(['/employees']);
      });
  }
  deleteEmployee(id: number) {
    this.http
      .delete(environment.API_PREFIX + '/Employee/' + id)
      .subscribe((res) => {
        localStorage.setItem('token', 'true');
        this.router.navigate(['/employees']);
      });
  }
}
