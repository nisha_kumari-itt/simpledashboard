import { EmployeesService } from 'src/app/service/employees.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './service/auth.service';
import { Router } from '@angular/router';
import { User } from './Interfac.';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'sampledashboard1';
  user: User = {
    fullname: '',
    emailId: '',
    phonenumber: 0,
    password: '',
    cpassword: '',
    id: 0,
    inlineRadioOption: '',
  };
  constructor(
    public auth: AuthService,
    private employees: EmployeesService,
    private router: Router
  ) {}
  ngOnInit() {
    let data = localStorage.getItem('user');
    if (data) {
      this.auth.user = JSON.parse(data);
    }
    this.auth.User.subscribe((res) => {
      this.user = res;
    });
    this.employees.getEmployees();
  }
  logout() {
    this.auth.user = {
      fullname: '',
      emailId: '',
      phonenumber: 0,
      password: '',
      cpassword: '',
      id: 0,
      inlineRadioOption: '',
    };
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }
}
