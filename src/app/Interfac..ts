export interface EmployeeInterface {
  firstName: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  salary: string;
  id: number;
}
export interface User {
  fullname: string;
  emailId: string;
  phonenumber: number;
  password: string;
  cpassword: string;
  inlineRadioOption: string;
  id: number;
}
