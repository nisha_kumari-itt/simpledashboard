import { EmployeesService } from 'src/app/service/employees.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { EmployeeInterface } from 'src/app/Interfac.';
@Component({
  selector: 'app-employee-chart',
  templateUrl: './employee-chart.component.html',
  styleUrls: ['./employee-chart.component.css'],
})
export class EmployeeChartComponent implements OnInit {
  canvas: any;
  ctx: any;
  @ViewChild('mychart') mychart: any;
  constructor(private employeesService: EmployeesService) {}
  employees: EmployeeInterface[] = [];
  ngOnInit(): void {
    this.employeesService.Employees.subscribe((res) => {
      this.employees = res;
      let data = [0, 0, 0, 0, 0];
      this.employees.forEach((ele) => {
        let num = Math.floor(<number>(<unknown>ele.salary) / 100000 / 5);
        data[num] = data[num] + 1;
      });
      this.generateChart(data);
    });
    this.employeesService.getEmployees();
  }
  generateChart(dataList: number[]) {
    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    new Chart(this.ctx, {
      type: 'bar',
      data: {
        datasets: [
          {
            label: 'Current Vallue',
            data: dataList,
            backgroundColor: 'rgb(115 185 243 / 65%)',
            borderColor: '#007ee7',
          },
        ],
        labels: ['0-5 LPA', '5-10 LPA', '10-15 LPA', '15-20 LPA', '20-25 LPA'],
      },
    });
  }
}
