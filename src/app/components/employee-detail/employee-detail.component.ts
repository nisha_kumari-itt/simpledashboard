import { EmployeesService } from 'src/app/service/employees.service';
import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EmployeeInterface } from 'src/app/Interfac.';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css'],
})
export class EmployeeDetailComponent implements OnInit {
  formValue = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('[a-zA-Z]+'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('[a-zA-Z]+'),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.pattern(
        '^([a-zA-Z0-9_.-])+@+([a-zA-Z0-9-])+.+[a-zA-Z0-9]{2,4}$'
      ),
    ]),
    mobileNumber: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10),
      Validators.pattern('[0-9]+'),
    ]),
    salary: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]+'),
    ]),
  });
  user: EmployeeInterface = {
    firstName: '',
    lastName: '',
    email: '',
    mobileNumber: '',
    salary: '',
    id: 0,
  };
  updateEmpValue: string = '';
  constructor(
    private authService: AuthService,
    private employeeService: EmployeesService,
    private activatedRoute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.employeeService.employee.subscribe((res) => {
      console.log(res);
      this.user = res;
      this.setFormValue();
    });

    this.activatedRoute.params.subscribe((params) => {
      this.employeeService.getEmployeeById(<number>params['id']);
    });
  }
  onSubmit() {
    if (this.formValue.dirty && this.formValue.valid) {
      this.employeeService.updateEmployee(this.formValue.value, this.user.id);
      alert('Updated Sucsessfully ');
    } else {
      alert('Changes Required');
    }
  }
  deleteEmployee() {
    if (confirm('Do you want to delete this Employee records?')) {
      this.employeeService.deleteEmployee(this.user.id);
    }
  }
  setFormValue() {
    this.formValue.get('firstName')?.setValue(this.user.firstName);
    this.formValue.get('lastName')?.setValue(this.user.lastName);
    this.formValue.get('email')?.setValue(this.user.email);
    this.formValue.get('mobileNumber')?.setValue(this.user.mobileNumber);
    this.formValue.get('salary')?.setValue(this.user.salary);
  }
}
