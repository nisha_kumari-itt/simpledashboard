import { HttpClient } from '@angular/common/http';
import { AuthService } from './../../service/auth.service';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  userList: any;
  returnUrl: string = '/employees';
  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {}
  loginUser(loginForm: NgForm) {
    if (
      loginForm.value.email === '' ||
      loginForm.value.password.trim() === ''
    ) {
      alert('Enter Your Email and Password');
    } else {
      this.http
        .get(environment.API_PREFIX + '/SignupUsers')
        .subscribe((users: any) => {
          this.userList = users;
          this.authService.login(loginForm);
          if (
            this.userList.find(
              (user: any) =>
                user.emailId == loginForm.value.email &&
                user.password == loginForm.value.password
            )
          ) {
          } else {
            alert('Incorrect Email or password');
          }
        });
    }
  }
}
