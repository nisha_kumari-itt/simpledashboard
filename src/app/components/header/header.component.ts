import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(public auth: AuthService, private router: Router) {}
  logout() {
    this.auth.user = {
      fullname: '',
      emailId: '',
      phonenumber: 0,
      password: '',
      cpassword: '',
      id: 0,
      inlineRadioOption: '',
    };
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }
}
