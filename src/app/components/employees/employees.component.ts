import { Component, ElementRef, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeInterface } from 'src/app/Interfac.';
import { EmployeesService } from 'src/app/service/employees.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
})
export class EmployeesComponent implements OnInit {
  formValue = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('[a-zA-Z]+'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.pattern('[a-zA-Z]+'),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.pattern(
        '^([a-zA-Z0-9_.-])+@+([a-zA-Z0-9-])+.+[a-zA-Z0-9]{2,4}$'
      ),
    ]),
    mobileNumber: new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10),
      Validators.pattern('[0-9]+'),
    ]),
    salary: new FormControl(''),
  });
  employees: EmployeeInterface[] = [];
  employeesAll: EmployeeInterface[] = [];
  emailMessage: boolean = false;
  constructor(
    private employeesService: EmployeesService,
    private router: Router,
    searchElement: ElementRef
  ) {}
  ngOnInit(): void {
    this.employeesService.Employees.subscribe((res) => {
      this.employees = <EmployeeInterface[]>res;
      this.employeesAll = <EmployeeInterface[]>res;
    });
    this.employeesService.getEmployees();
  }
  onSubmit() {
    console.log(this.formValue);
    if (
      this.formValue.valid &&
      this.formValue.get('mobileNumber')?.value.toString().length == 10 &&
      this.employeesService.getEmployeeByEmail(
        this.formValue.get('email')?.value.toString()
      )
    ) {
      this.employeesService
        .postEmployee(this.formValue.value)
        .subscribe((res) => {
          this.employeesService.getEmployees();
        });
      this.formValue.reset();
      alert('Employee Added');
      this.emailMessage = true;
    } else {
      this.emailMessage = false;
    }
  }
  employeeDetail(id: number) {
    this.router.navigate(['/employeeDetails', id]);
  }
  clearSearch(empstr: HTMLInputElement) {
    empstr.value = '';
    this.employees = this.employeesAll;
  }
  onSearch(empstr: HTMLInputElement) {
    const empstring = empstr.value;
    console.log(empstr);
    this.employees = [];
    this.employeesAll.forEach((emp) => {
      if (
        emp.firstName.toLowerCase().includes(empstring.toLowerCase()) ||
        emp.lastName.toLowerCase().includes(empstring.toLowerCase())
      ) {
        this.employees.push(emp);
      }
    });
  }
}
