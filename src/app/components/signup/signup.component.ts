import { User } from './../../Interfac.';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  inlineRadioOptions: string = '';
  flagValidate: boolean = false;
  constructor(private http: HttpClient, private router: Router) {}
  signupUser(signUpForm: NgForm) {
    console.log(signUpForm.value.inlineRadioOptions);
    if (
      signUpForm.value.fullname === '' ||
      signUpForm.value.email === '' ||
      signUpForm.value.number === '' ||
      signUpForm.value.password === '' ||
      signUpForm.value.cpassword === ''
    ) {
      alert('Fill Up the Form');
    } else if (signUpForm.value.inlineRadioOptions === '') {
      alert('please select the gender');
    } else {
      let isValid = true;
      if (signUpForm.value.password != signUpForm.value.cpassword) {
        alert('Password and Confirm Password Should be Same ');
        isValid = false;
      } else if (signUpForm.value.password == signUpForm.value.cpassword) {
        isValid = true;
        if (isValid) {
          this.http
            .get(environment.API_PREFIX + '/SignupUsers')
            .subscribe((empres) => {
              let signup = true;
              (<User[]>empres).forEach((ele) => {
                if (ele.emailId == signUpForm.value.emailId) {
                  signup = false;
                }
              });
              if (signup) {
                this.http
                  .post<any>(
                    environment.API_PREFIX + '/SignupUsers',
                    signUpForm.value
                  )
                  .subscribe(
                    (res) => {
                      console.log(signUpForm);
                      alert('Signup successfull');
                      signUpForm.reset();
                      this.router.navigate(['/login']);
                    },
                    (err) => {
                      alert('Something went wrong');
                    }
                  );
              } else {
                alert('Email already exists.');
              }
            });
        }
      }
    }
  }
}
