import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'salary',
})
export class SalaryPipe implements PipeTransform {
  transform(value: unknown): unknown {
    return 'Rs. ' + value;
  }
}
